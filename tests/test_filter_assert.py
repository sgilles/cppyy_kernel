import unittest

from cppyy_kernel import CppyyKernel

class CppyyKernelTests(unittest.TestCase):
    kernel_name = "cppyy"

    include_no_assert = ('#include <iostream>', '', '', 'std::cout << "Hello world!" << std::endl')
    include_cassert = ('#include <iostream>', '#include <cassert>', '', 'std::cout << "Hello world!" << std::endl')
    include_assert_h = ('#include <iostream>', '#include <assert.h>', '', 'std::cout << "Hello world!" << std::endl')
    include_cassert_twice = ('#include <iostream>', '#include <cassert>', '#include <cassert>', 'std::cout << "Hello world!" << std::endl')
    
    def test_no_include_assert(self):
        
        code = "\n".join(self.include_no_assert)
        self.assertEqual(CppyyKernel._handle_assert(code), code)

    def test_include_cassert(self):

        code = "\n".join(self.include_cassert)
        expected_code = "\n".join(self.include_no_assert)
        self.assertEqual(CppyyKernel._handle_assert(code), expected_code)

    def test_include_assert_h(self):

        code = "\n".join(self.include_assert_h)
        expected_code = "\n".join(self.include_no_assert)
        self.assertEqual(CppyyKernel._handle_assert(code), expected_code)

    def test_include_cassert(self):

        code = "\n".join(self.include_cassert_twice)
        expected_code = "\n".join(self.include_no_assert)
        self.assertEqual(CppyyKernel._handle_assert(code), expected_code)


if __name__ == "__main__":
    unittest.main()
