The Dockerfile present here might seem needlessly obtuse and complicated, but we had to juggle with
several constraints:

- `cpp_cling` latest version was published in 2023, and doesn't work well on an Ubuntu 24.04.
- Python 3.12 is required, but Ubuntu 20.04 is shipped with Python 3.10. It is possible to update
the version (this is done in the Dockerfile) but it's not trivial to do.