# On August 2024, latest [cppyy-cling](https://pypi.org/project/cppyy-cling) happened before Ubuntu 24.04.
FROM ubuntu:22.04

LABEL maintainer="Sébastien Gilles - sebastien.gilles@inria.fr"

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q && apt-get -y -q autoclean && apt-get -y -q autoremove)

# Most recent versions supported by Ubuntu 22.04 - through a PPA for gcc.
ARG GCC_VERSION=13
ARG CLANG_VERSION=15

# Lots of these dependencies are there to install Python 3.12, required for CppyyKernel.
# See https://tecadmin.net/how-to-install-python-3-12-on-ubuntu
RUN apt-get install --no-install-recommends -y git wget clang-${CLANG_VERSION} clang-tools-${CLANG_VERSION} clang-tidy-${CLANG_VERSION} gpg-agent software-properties-common  build-essential libffi-dev libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev libssl-dev

RUN add-apt-repository ppa:ubuntu-toolchain-r/test && apt-get update \
     && apt-get install -y gcc-$GCC_VERSION g++-$GCC_VERSION

# To avoid the blocking question about geographic area.
ARG DEBIAN_FRONTEND=noninteractive

RUN add-apt-repository ppa:deadsnakes/ppa && apt-get update \
     && apt-get install -y python3.12 python3.12-dev python3.12-distutils

RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-$GCC_VERSION 100 --slave /usr/bin/g++ g++ /usr/bin/g++-$GCC_VERSION
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.12 312

RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-${CLANG_VERSION} 100 --slave  /usr/bin/clang++ clang++ /usr/bin/clang++-${CLANG_VERSION} --slave  /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-${CLANG_VERSION}

ARG PIP_DISABLE_PIP_VERSION_CHECK=1
ARG PIP_NO_CACHE_DIR=1

RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3.12 get-pip.py

# Note for pip: it is ok for current version but more recent ones complain
# about externally managed environment.
# I found a working path on
# https://stackoverflow.com/questions/77028925/docker-compose-fails-error-externally-managed-environment:
# --break-system-packages that I had to use when using a more recent Ubuntu than 22.04
RUN python3.12 -m pip install jupyterlab

COPY . cppyy_kernel

# If we don't specify this cppyy_kernel infers an architecture specific gcc which isn't found...
ARG CC=gcc
ARG CXX=g++

# Make RUN commands use the new environment:
SHELL ["/bin/bash", "-c"]

WORKDIR /cppyy_kernel

RUN python3.12 -m pip install .

# The code to run when container is started:
ENTRYPOINT [ "jupyter", "lab", "--port=8888", "--ip=0.0.0.0", "--no-browser","--allow-root"]
