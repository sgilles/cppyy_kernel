import cppyy
import sys
import time
import base64
from subprocess import Popen, PIPE
import subprocess
from tempfile import TemporaryDirectory
from pathlib import Path
from typing import TYPE_CHECKING, Any
if TYPE_CHECKING:
    from .kernel import CppyyKernel
from cppyy_kernel.util import kernel_extension, CLANG_COMMAND_LIST, GCC_COMMAND_LIST

DATA_LENGTH_LIMIT = 10_000


@kernel_extension(pattern="std::cin/cppyy")
def do_execute_stdcin_cppyy(kernel: "CppyyKernel", code: str, **kw: Any) -> None:
    """
    A hacky way to make cppyy interact with standard input through the notebook interface.

    It is intended for use only on simple training examples; the code cell must be kernel-sufficient
    (the previous cells execution context is ignored).

    You should probably consider using std::cin/clang or std::cin/gcc instead.
    """
    bcode = base64.b64encode(code.encode()).decode()
    exec_list = [sys.executable, __file__, bcode]
    return _execute_stdcin_impl(kernel, exec_list)


@kernel_extension(pattern="std::cin/clang")
def do_execute_stdcin_clang(kernel: "CppyyKernel", code: str, **kw: Any) -> None:
    """
    As 'clang' magics, with additional work to enable standard input interaction through the notebook interface.

    It is enabled but is still mostly a hack; we do not recommend it over 'clang' if you do not need to interact with std::cin.
    """
    return do_execute_stdcin_compiler(kernel, code, list(CLANG_COMMAND_LIST))


@kernel_extension(pattern="std::cin/gcc")
def do_execute_stdcin_gcc(kernel: "CppyyKernel", code: str, **kw: Any) -> None:
    """
    As 'gcc' magics, with additional work to enable standard input interaction through the notebook interface.

    It is enabled but is still mostly a hack; we do not recommend it over 'gcc' if you do not need to interact with std::cin.
    """
    return do_execute_stdcin_compiler(kernel, code, list(GCC_COMMAND_LIST))


def do_execute_stdcin_compiler(kernel: "CppyyKernel", code: str, exec_list: list[str]) -> None:
    """
    The implementation used to handled std::cin/clang and std::cin/gcc.

    @param code The content of the cell, minus the magics line.
    @param exec_list The command to invoke the compiler, given as a list. For instance
    CLANG_COMMAND_LIST or GCC_COMMAND_LIST, defined in utils.py.
    """
    main_prog = f"{code}\n"
    with TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        cpp_file = "cpp_file.cpp"
        cpp_path = tmpdir / cpp_file
        a_out = tmpdir / "a.out"
        with open(cpp_path, "w") as cppf:
            cppf.write(main_prog)
        # kernel.Print(main_prog)
        completed = subprocess.run(exec_list + [cpp_file],
                                   cwd=tmpdir, capture_output=True, text=True)
        if err := completed.stderr:
            kernel.Error(err)
        kernel.Print(completed.stdout)
        exec_list = [a_out]  # type: ignore
        return _execute_stdcin_impl(kernel, exec_list)


def _execute_stdcin_impl(kernel: "CppyyKernel", exec_list: list[str]) -> None:
    with TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        outpath = tmpdir / "stdout_"
        errpath = tmpdir / "stderr_"
        with open(outpath, "wb") as woutfile, open(errpath, "wb") as werrfile:
            proc = Popen(
                exec_list,
                stdin=PIPE,
                stdout=woutfile,
                stderr=werrfile,
            )
            with open(outpath, "rb") as outfile, open(errpath, "rb") as errfile:
                while True:
                    time.sleep(0.3)
                    out = ""
                    cnt = 15
                    while out == "":
                        if not cnt:
                            break
                        time.sleep(0.1)
                        out += outfile.read(DATA_LENGTH_LIMIT).decode()
                        cnt -= 1
                    kernel.Print(out)
                    if len(out) >= DATA_LENGTH_LIMIT:  # equals actually
                        kernel.Error("Probably an incorrect value entered ...")
                        break
                    if err := errfile.read():
                        err_ = err.decode()
                        kernel.Error(err_)
                    proc.poll()
                    if proc.returncode is not None:
                        kernel.PrintKernelInformation("Done!")
                        return
                    raw_inp = kernel.raw_input(prompt=">")
                    resp = f"{raw_inp}\n".encode()
                    try:
                        assert proc.stdin is not None
                        proc.stdin.write(resp)
                        proc.stdin.flush()
                    except Exception as exc:
                        kernel.Error(exc.args[0], end="\n")
                        break
            proc.kill()


def main() -> None:
    bcode = sys.argv[1]
    code = base64.b64decode(bcode).decode()
    cppyy.cppexec(code)


if __name__ == "__main__":
    main()
