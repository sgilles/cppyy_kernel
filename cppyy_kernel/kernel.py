from metakernel import MetaKernel
import subprocess
import shutil
from tempfile import TemporaryDirectory
from pathlib import Path
import cppyy
import io
import re
from contextlib import redirect_stderr, redirect_stdout
from wurlitzer import sys_pipes
from IPython.display import HTML
from ._stdcin import do_execute_stdcin_cppyy, do_execute_stdcin_clang  # noqa: F401
from .compiler import do_execute_clang, do_execute_gcc  # noqa: F401
from . import __version__
from .util import extensions, kernel_extension, Relay, CPP_STANDARDS, CLANG, GCC
from .magics import CppMagics
from typing import Any


class CppyyKernel(MetaKernel):
    implementation = "Cppyy"
    implementation_version = __version__
    language = "c++"
    language_version = f"cppyy_{cppyy.__version__}"
    language_info = {
        "name": "c++",
        "codemirror_mode": "c++",
        "mimetype": "text/x-c++src",
        "file_extension": ".cpp",
    }
    banner = "Cppyy kernel"

    # Letting the initial '?' led to spurious behaviour when command was ended by a '?',
    # even if in a comment.
    help_suffix = "_We don't want any!_"

    REGEX_INCLUDE_ASSERT = re.compile(r"#\s*include.*<(c|)assert.*>")

    FILTER_STANDARD_ERROR_LIST = (
        "compilation failed",
        "Failed to parse the given C++ code",
        # Next three lines are related to a JIT issue in macOS; I didn' manage to
        # use properly the suggested macro value.
        'Warning: uncaught exception in JIT is rethrown; resources may leak (suppress with "CPPYY_UNCAUGHT_QUIET=1")',  # macOS ARM warning
        "long CppyyLegacy::TInterpreter::ProcessLine(const char* line, int* error = 0) =>",
        "    exception: ",
        "0.00s -",
    )

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.register_magics(CppMagics)
        self._cache: dict[str, str] = {}
        self.gcc_std: str | None = None
        self.clang_std: str | None = None
        self.sniff_compilers()

    @classmethod
    def _handle_assert(cls, code: str) -> str:
        """
        Hack to handle C++ assert with cppyy kernel: cling is compiled with optimization mode, so NDEBUG is
        defined and assert are therefore empty.

        To circumvent that, we:
        - Define ourselves the `assert` macro, which prints something on std::cerr and
        exit.
        - Skim through the code provided in the cell to remove any occurrence of include
        that would superseded our macro.

        This is not a full proof solution: if a STL include includes itself <cassert>
        our macro will be superseded. But it nonetheless improve the situation for our
        use case: explain in our C++ training support the role of assert in C++ code.

        @return Same as input code except that the lines including assert having removed
        (if any).

        This method shouldn't be called if the cell is not handled by cppyy (typically for cells
        that use up compilers, e.g. `cppmagics clang`).
        """
        try:
            cppyy.cppdef(
                "#include <exception>\n#define assert(b) { if (!(b)) { "
                'std::cerr << "Assert (handled here by notebook kernel): " '
                "<< #b << std::endl;; throw std::exception();; }}"
            )
            return code.replace(
                cls.REGEX_INCLUDE_ASSERT.search(code).group(0), ""  # type: ignore
            )
        except AttributeError:
            return code

    def _discard_line(self, line: str) -> bool:
        """
        Helper method for _filter_standard_error().
        """

        for item in self.FILTER_STANDARD_ERROR_LIST:
            if line.startswith(item):
                return True
        return False

    def _filter_standard_error(self, content: str) -> str:
        """
        cppyy-backend writes in its implementation a message on the standard error, which
        is not appropriate in all cases (for instance when an exception is thrown we get
        an inappropriate "compilation failed" message).

        We therefore filter out this "compilation failed" line.

        We also filter out a SyntaxError message "Failed to parse the given C++ code"
        provided by cppyy that might appear when that's clearly not the case (e.g. when
        an exception is thrown or if we use an undeclared C++ variable).

        @return Same `content` with "Failed to parse the given C++ code" and "compilation failed"
        lines removed.
        """
        filtered_content = [
            line for line in content.split("\n") if not self._discard_line(line)
        ]
        return "\n".join(filtered_content).lstrip("\n")

    def _run_cpp_cell(self, cell_content: str, info: dict[str, Any]) -> bool:
        """
        Run the C++ code given in a cell.

        cppyy provides two commands:
        - cppdef, to provide C++ code to be compiled.
        - cppexec, to provide C++ code to be compiled and executed.

        For our needs, cppexec is the most obvious choice as a C++ cell may be both.
        However, cppyy authors recommend using the former when it's not executable code,
        and at least forward declaration isn't properly taken into account properly by
        cppexec.

        @return Return of the cppyy command used.
        """
        if info.get("use_cppdef", False):
            return cppyy.cppdef(cell_content)
        else:
            return cppyy.cppexec(cell_content)

    def Error(self, *objects: Any, **kwargs: Any) -> None:
        """
        Specialize Metakernel `Error` method by filtering specific errors we don't want
        to see in the notebooks.
        """
        if filtered := self._filter_standard_error(*objects):
            return super().Error(filtered, **kwargs)

        return

    def do_execute_direct(self, code: str, **kwargs: Any) -> Any:
        """
        Execute the C++ code in the given cell with cppyy.
        code:
            contains the cell code
        kwargs:
            provides various parameters from magics (currently cppmagics)
        @param use_cppdef By default we used cppyy.cppexec command to run a cell,
        but in some cases we want explicitly cppyy.cppdef to be used (see
        _run_cpp_cell() for more details about that.
        """
        if not code.strip(" \r\n"):
            return

        if (relay := self.process_extension(code, kwargs)) and relay.finished():
            return

        code = self._handle_assert(relay.code)

        lcode = code.split("\n")
        if lcode[-1].startswith("#include") and code[-1] != "\n":
            code += "\n"  # avoids a warning "extra tokens at end of #include directive"
        buff = io.StringIO()
        buff_err = io.StringIO()
        ret = None
        out = None
        with (
            redirect_stderr(buff_err),
            redirect_stdout(buff),
        ):  # actually only stderr is used
            try:
                # from https://notebook.community/minrk/wurlitzer/Demo
                with sys_pipes() as (stdout, _):
                    ret = self._run_cpp_cell(code, relay.info)
                if out := stdout.getvalue().strip("\r\n"):
                    self.Print(out, end="")

            except Exception as exc:
                if exc.args[0]:
                    self.Error(exc.args[0], end="")

        if (bout := buff.getvalue().strip("\r\n")) and bout != out:
            self.Print(bout, end="")

        if berr := buff_err.getvalue():
            self.Error(berr, end="")

        return None if ret is True else ret

    def process_extension(self, code: str, xargs: dict[str, Any]) -> Relay:
        if "magics" in xargs:
            magics = xargs["magics"]
            assert "toolname" in magics
            toolname = magics["toolname"]
            toolargs = magics["toolargs"]
            kw = {"print_command": magics["print_command"]}
            if toolname not in extensions:
                if toolname:
                    self.Error(f"Unknown cppmagics tool: {toolname}")
                self.Print("Currently the following tools are available:")
                data = dict(
                    sorted(
                        [
                            (key, fnc.__doc__.strip(" ").replace("\n", " "))  # type: ignore
                            for (key, fnc) in extensions.items()
                        ]
                    )
                )
                sio = io.StringIO("<dl>")
                for dt, dd in data.items():
                    sio.write(f"<dt>{dt}<dt><dd>{dd}</dd>")
                sio.write("</dl>")
                self.Display(HTML(sio.getvalue()))  # type: ignore
                return Relay(code="")  # i.e. process finished
            ret = extensions[toolname](
                self, code, *((toolargs,) if toolargs else ()), **kw
            )
            return Relay(code="") if ret is None else ret
        return Relay(code=code)

    def PrintKernelInformation(self, text: str) -> None:
        """
        Print in a different color messages giving away information about the kernel
        operations done under the hood.
        """
        CYAN = "\033[96m"
        ENDC = "\033[0m"
        self.Print(f"{CYAN} [Kernel] {text} {ENDC}")

    def PrintKernelError(self, text: str) -> None:
        """
        Print in a different color messages giving away errors about the kernel
        operations done under the hood.
        """
        FAIL = "\033[91m"
        ENDC = "\033[0m"
        self.Print(f"{FAIL} [Kernel] {text} {ENDC}")

    def PrintKernelWarning(self, text: str) -> None:
        """
        Print in a different color messages giving away warnings about the kernel
        operations done under the hood.
        """
        WARNING = "\033[93m"
        ENDC = "\033[0m"
        self.Print(f"{WARNING} [Kernel] {text} {ENDC}")

    def PrintTerminalCommand(self, text: str) -> None:
        """
        Print in a different color messages giving away the command that would be
        used in a terminal.
        """
        TERM = "\033[95m"
        ENDC = "\033[0m"

        self.Print(f"{TERM} [Terminal command] {text} {ENDC}")

    def _check_compiler_std(self, compiler: str, compiler_std: str,
                            tmpdir: Path, cpp_file: Path, o_file: Path) -> bool:
        """
        This function determine if the compilation error is due to an -std option
        not supported by the compiler and propose to change it if necessary via
        a dialog box
        """
        completed = subprocess.run(
            [compiler, "-O0", f"-std={compiler_std}", "-o", o_file, cpp_file],
            cwd=tmpdir,
            capture_output=True,
            text=True,
        )
        if not completed.stderr:
            return True
        return False

    def get_std(self, compiler: str) -> str | None:
        if compiler == GCC:
            return self.gcc_std
        assert compiler == CLANG
        return self.clang_std

    def set_std(self, compiler: str, std: str) -> None:
        if compiler == GCC:
            self.gcc_std = std
        else:
            assert compiler == CLANG
            self.clang_std = std

    def sniff_compilers(self) -> None:
        self.Print(".......................................")  # do not remove
        self.PrintKernelInformation("Sniffing compilers ...")
        with TemporaryDirectory() as tmpdirname:
            tmpdir = Path(tmpdirname)
            cpp_file = tmpdir / "dummy.cpp"
            o_file = tmpdir / "dummy.out"
            with open(cpp_file, "w") as f:
                f.write("int main(){}")
            for compiler in CPP_STANDARDS.keys():
                if not shutil.which(compiler):
                    self.PrintKernelWarning(f"{compiler} was not found!")
                    continue
                for std_candidate in CPP_STANDARDS[compiler]:
                    if self._check_compiler_std(compiler, std_candidate,
                                                tmpdir, cpp_file, o_file):
                        self.set_std(compiler, std_candidate)
                        self.PrintKernelInformation(
                            f"The selected standard for {compiler} is {std_candidate}"
                        )
                        break
                else:
                    self.PrintKernelWarning(f"Your {compiler} is probably too old!")


@kernel_extension(pattern="cppyy/cppdef")
def do_execute_cppyy_cppdef(kernel: CppyyKernel, code: str, **kw: Any) -> Relay:
    """
    Plugin to use cppyy.cppdef instead of cppyy.cppexec for the current cell.

    This is for instance required if you want to declare a forward declaration.
    """
    return Relay(code=code, info=dict(use_cppdef=True))
