from ipykernel.kernelapp import IPKernelApp
from . import CppyyKernel

IPKernelApp.launch_instance(kernel_class=CppyyKernel)
