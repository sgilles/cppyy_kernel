from dataclasses import dataclass, field
from typing import Callable, Any, TYPE_CHECKING

if TYPE_CHECKING:
    from .kernel import CppyyKernel


@dataclass
class Relay:
    code: str
    info: dict[str, Any] = field(default_factory=dict)

    def finished(self) -> bool:
        return not self.code


ExtensionFunc = Callable[["CppyyKernel", str], Relay | None]

extensions: dict[str, ExtensionFunc] = {}

GCC = "g++"
CLANG = "clang++"

CLANG_COMMAND_LIST: tuple[str, ...] = (
    CLANG,
    "-O0",
    "-Weverything",
    "-Wno-poison-system-directories",
    "-Wno-c++98-compat",
    "-Wno-c++20-compat",
    "-Wno-padded",
    "-Wno-weak-vtables",
    "-Wno-unknown-warning-option"
)

GCC_COMMAND_LIST: tuple[str, ...] = (
    GCC,
    "-O0",
    "-Wall",
    "-Wextra",
    "-Wno-unknown-warning"
)

# Possible gcc values from https://gcc.gnu.org/projects/cxx-status.html
# Possible clang values from https://clang.llvm.org/cxx_status.html
CPP_STANDARDS = {  # sorted by preference order
    GCC: ("c++2b", "c++20", "c++2a", "c++17", "c++14", "c++11", "c++98"),
    CLANG: ("c++2c", "c++23", "c++20", "c++2a", "c++17", "c++14", "c++11", "c++98"),
}


def kernel_extension(pattern: str) -> Callable[[ExtensionFunc], ExtensionFunc]:
    """
    When applied to a function, this decorator registers it with the kernel as a member
    of the %%cppmagics The function must have a signature compatible with the ExtensionFunc
    type (see above) therefore it must return:
    * None if the function performs complete processing
    OR
    * a `Relay` object for which `code` attribute is mandatory and which may contain other
    optional information in the `info` attribute (currently it may contains `use_cppdef` key)
    """

    def _wrapper(func: ExtensionFunc) -> ExtensionFunc:
        extensions[pattern] = func
        return func

    return _wrapper
