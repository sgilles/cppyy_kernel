import subprocess
import shutil
import shlex
import platform
from tempfile import TemporaryDirectory
from pathlib import Path
from cppyy_kernel.util import (
    kernel_extension,
    CLANG_COMMAND_LIST,
    GCC_COMMAND_LIST,
    CLANG,
    GCC
)
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from .kernel import CppyyKernel


def _do_call_compiler(
    kernel: "CppyyKernel",
    compiler_command_list: list[str],
    std: str,
    code: str,
    print_command: bool,
    extension: str = "cpp"
) -> None:
    """
    This function is called when we want to compile with an external compiler
    and then run code in a cell.

    Contrary to what we do with cppyy, the cell is self contained and must provide a
    `main()` function.

    @kernel Our cppyy kernel object.
    @param compiler_command_list Either CLANG_COMMAND_LIST or GCC_COMMAND_LIST.
    @param code Code read in the cell, to be compiled and then run.
    @param print_command If true, make the kernel print the command that would be typed
    in the terminal to provide the same result as the cell.
    @param extension Extension to give to the created file. The dot may be specified or not
    (internally if a dot is found as first character it is removed).
    """
    if not shutil.which(compiler_command_list[0]):
        kernel.PrintKernelError(
            f"Compiler {compiler_command_list[0]} was not found in PATH!"
        )
        return
    kernel.PrintKernelInformation(f"Compiling code with {compiler_command_list[0]}")
    main_prog = f"{code}\n"
    if extension.startswith("."):
        extension = extension[1:]
    with TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        cell_file = f"cell_file.{extension}"
        cell_path = tmpdir / cell_file
        a_out = tmpdir / "a.out"

        with open(cell_path, "w") as cppf:
            cppf.write(main_prog)
        cmd = compiler_command_list + [std] if std else compiler_command_list

        if print_command:
            pretty_cmd = " ".join((str(item) for item in cmd)) + " *file_with_cell_content.cpp*"
            kernel.PrintTerminalCommand(pretty_cmd)

        cmd.append(cell_file)
        completed = subprocess.run(cmd, cwd=tmpdir, capture_output=True, text=True)

        if err := completed.stderr:
            kernel.Error(err)
        if completed.stdout:
            kernel.Print(completed.stdout)

        if completed.returncode == 0:
            kernel.PrintKernelInformation("Compilation went fine!")
            _run_executable(kernel, a_out)
        else:
            kernel.PrintKernelError("Compilation failed!")


@kernel_extension(pattern="anycppcompiler")
def do_execute_any_compiler(
    kernel: "CppyyKernel", code: str, arg: str = "", extension: str = "cpp", **kw: Any
) -> None:
    """
    Compile the code in the cell using a compiler (and its options) you have to
    provide through a dialog box.

    It is assumed the code is C++ (the file generated to is given by default a cpp extension).

    If `-p` flag is provided between `%%cppmagics` and `anycppcompiler`, the
    command as it would be typed in a terminal is printed.

    """
    print_command = kw.get("print_command", False)
    cmd = arg or kernel.raw_input("compiler command (no file)>")
    cmd_list = shlex.split(cmd)
    _do_call_compiler(kernel, cmd_list, "", code, print_command, extension)


@kernel_extension(pattern="clang")
def do_execute_clang(kernel: "CppyyKernel", code: str, **kw: Any) -> None:
    """
    Compile the code in the cell using clang compiler and some default options.

    These options are given by `CLANG_COMMAND_LIST` and `CLANG` variables.

    If you want to use others, consider using `anycppcompiler` extension.

    If `-p` flag is provided between `%%cppmagics` and `anycppcompiler`, the
    command as it would be typed in a terminal is printed.
    """
    print_command = kw.get("print_command", False)

    clang_std = kernel.get_std(CLANG)
    _do_call_compiler(kernel, list(CLANG_COMMAND_LIST), f"-std={clang_std}", code, print_command=print_command)


@kernel_extension(pattern="gcc")
def do_execute_gcc(kernel: "CppyyKernel", code: str, **kw: Any) -> None:
    """
    Compile the code in the cell using gcc compiler and some default options.

    These options are given by `GCC_COMMAND_LIST` and `GCC` variables.

    If you want to use others, consider using `anycppcompiler` extension.

    If `-p` flag is provided between `%%cppmagics` and `anycppcompiler`, the
    command as it would be typed in a terminal is printed.
    """
    print_command = kw.get("print_command", False)

    gcc_std = kernel.get_std(GCC)
    if platform.system() == "Darwin":
        kernel.PrintKernelWarning(
            "On macOS, g++ might be an alias to clang++. "
            "Please check with `!g++ -v` to make sure you're "
            "really using gcc!"
        )
    _do_call_compiler(kernel, list(GCC_COMMAND_LIST), f"-std={gcc_std}", code, print_command=print_command)


@kernel_extension(pattern="clang-tidy")
def do_execute_clang_tidy(
    kernel: "CppyyKernel", code: str, arg: str = "", **kw: Any
) -> None:
    """
    Apply clang-tidy on the given cell.

    This command requires that `clang-tidy` may be found in the ${PATH} environment variable.

    `clang-tidy` is run on the content of the current cell; it is expected that clang-tidy options
    are provided either interactively or through a magics argument.

    Typically the expected options are:
    - `--checks="*your choices*"`
    - `--fix` if you intend to use this functionality.

    In case the `--fix` flag is there, the new content of the cell after correction is given in output.

    If `-p` or `--print-command` is provided _before_ the `clang-tidy` magics, the command line
    as it would be typed in a terminal is printed. For instance:

    ```
    %%cppmagics -p clang-tidy -checks="*"
    ```
    """
    exec_name = "clang-tidy"

    input_ = arg or kernel.raw_input("Please include clang-tidy options: ")
    input_as_list = shlex.split(input_)

    is_fix_flag = "--fix" in input_as_list

    if not shutil.which(exec_name):
        kernel.PrintKernelError(f"{exec_name} was not found in PATH!")
        return

    main_prog = f"{code}\n"

    with TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        cell_path = tmpdir / "cell_file.cpp"

        with open(cell_path, "w") as cppf:
            cppf.write(main_prog)

        cmd_list = [
            exec_name,
        ]
        cmd_list.append(cell_path)  # type: ignore
        cmd_list.extend(input_as_list)
        len_after_input = len(cmd_list)
        warning_list = (item for item in CLANG_COMMAND_LIST if item.startswith("-W"))
        cmd_list.append("--")
        cmd_list.extend(warning_list)
        cmd_list.append(f"-std={kernel.get_std(CLANG)}")
        cmd_list.extend(("-x", "c++"))

        if kw.get("print_command", False):
            # 'cmd_list' has been prepared for subprocess, but may have lost quotes in the process.
            # As we want to give the exact command that would have been typed in the terminal, we prepare a more
            # adequate list.
            pretty_cmd_list = (
                [exec_name, "*file_with_cell_content.cpp*"]
                + [input_]
                + cmd_list[len_after_input:]
            )
            pretty_cmd = " ".join((str(item) for item in pretty_cmd_list))

            kernel.PrintTerminalCommand(pretty_cmd)

        kernel.PrintKernelInformation("Output is: ")

        completed = subprocess.run(cmd_list, cwd=tmpdir, capture_output=True, text=True)

        if err := completed.stderr:
            kernel.Error(err)

        if completed.stdout:
            kernel.Print(completed.stdout)

        if completed.returncode == 0:
            kernel.PrintKernelInformation("clang-tidy command went fine!")
            if is_fix_flag:
                kernel.PrintKernelInformation(
                    "After the automated fixes are applied code looks like:"
                )

                with open(cell_path, "r") as cppf:
                    for line in cppf:
                        kernel.Print(line.rstrip())

        else:
            kernel.PrintKernelError("clang-tidy command failed!")


def _run_executable(kernel: "CppyyKernel", a_out: str | Path) -> None:
    kernel.PrintKernelInformation("Running code; output is:")
    kernel.do_execute(f"%shell {a_out}")
